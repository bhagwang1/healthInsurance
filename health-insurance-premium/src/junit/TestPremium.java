package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import beans.CurrentHealth;
import beans.Habits;
import beans.Person;
import service.HealthPremiumService;

public class TestPremium {
	
	HealthPremiumService hps= new HealthPremiumService();
	Person person = new Person();
	Habits habits= new Habits();
	CurrentHealth ch= new CurrentHealth();

	@Test
	public void testWithAge() {
		person.setAge(34);
		int l= (int)hps.calculateHealthPremium(person);
		assertEquals(6500,l);
	}
	@Test
	public void testWithAgeGender() {
		person.setAge(34);
		int l= (int)hps.calculateHealthPremium(person);
		assertEquals(6500,l);
	
	
		person.setGender("male");
		int l1= (int)hps.calculateHealthPremium(person);
		assertEquals(6630,l1);
	}
		
	@Test
	public void testWithAgeGenderCurrentHealth() {
		person.setAge(34);
		int l= (int)hps.calculateHealthPremium(person);
		assertEquals(6500,l);
	
	
		person.setGender("male");
		int l1= (int)hps.calculateHealthPremium(person);
		assertEquals(6630,l1);
		
		ch.setBloodPressure(false);
		ch.setHypertension(false);
		ch.setBloodSugar(false);
		ch.setOverweight(true);
		person.setCurHealth(ch);
		int l2= (int)hps.calculateHealthPremium(person);
		assertEquals(6696,l2);
		
		ch.setBloodPressure(false);
		ch.setHypertension(false);
		ch.setBloodSugar(true);
		ch.setOverweight(true);
		person.setCurHealth(ch);
		int l3= (int)hps.calculateHealthPremium(person);
		assertEquals(6762,l3);
	}
		
	@Test
	public void testWithAgeGenderHabits() {
		person.setAge(34);
		int l= (int)hps.calculateHealthPremium(person);
		assertEquals(6500,l);
	
	
		person.setGender("male");
		int l1= (int)hps.calculateHealthPremium(person);
		assertEquals(6630,l1);
		
		habits.setAlcohol(true);
		habits.setDrugs(false);
		habits.setDailyExercise(true);
		habits.setSmoking(false);
		person.setHabits(habits);
		int l4= (int)hps.calculateHealthPremium(person);
		assertEquals(6624,l4);
		
		habits.setAlcohol(true);
		habits.setDrugs(true);
		habits.setDailyExercise(true);
		habits.setSmoking(false);
		person.setHabits(habits);
		int l5= (int)hps.calculateHealthPremium(person);
		assertEquals(6822,l5);
		
	}
	
	//tearDown used to close the connection or clean up activities
	   public void tearDown(  ) {
	   }
		
}

