package beans;

public class CurrentHealth extends Person{

	Long currentHealthId;
	
	Boolean hypertension;
	Boolean bloodPressure;
	Boolean bloodSugar;
	Boolean overweight;
	public Long getCurrentHealthId() {
		return currentHealthId;
	}
	public void setCurrentHealthId(Long currentHealthId) {
		this.currentHealthId = currentHealthId;
	}
	public Boolean getHypertension() {
		return hypertension;
	}
	public void setHypertension(Boolean hypertension) {
		this.hypertension = hypertension;
	}
	public Boolean getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(Boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public Boolean getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(Boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public Boolean getOverweight() {
		return overweight;
	}
	public void setOverweight(Boolean overweight) {
		this.overweight = overweight;
	}
	
	
}
