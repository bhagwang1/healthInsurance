package beans;

public class Person {
	
	Long id;
	String name;
	String gender;
	int age;
	CurrentHealth curHealth;
	Habits habits;
	
	public CurrentHealth getCurHealth() {
		return curHealth;
	}
	public void setCurHealth(CurrentHealth curHealth) {
		this.curHealth = curHealth;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	

}
