package beans;

public class Habits extends Person{
	
	Long habitsId;
	
	Boolean Smoking;
	Boolean alcohol;
	Boolean dailyExercise;
	Boolean drugs;
	public Long getHabitsId() {
		return habitsId;
	}
	public void setHabitsId(Long habitsId) {
		this.habitsId = habitsId;
	}
	public Boolean getSmoking() {
		return Smoking;
	}
	public void setSmoking(Boolean smoking) {
		Smoking = smoking;
	}
	public Boolean getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(Boolean alcohol) {
		this.alcohol = alcohol;
	}
	public Boolean getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(Boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public Boolean getDrugs() {
		return drugs;
	}
	public void setDrugs(Boolean drugs) {
		this.drugs = drugs;
	}

}
