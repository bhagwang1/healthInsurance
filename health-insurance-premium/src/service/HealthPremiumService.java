package service;

import beans.Person;

public class HealthPremiumService {
	
	int premiumAmount;
	
	public int calculateHealthPremium(Person person) {
		
		premiumAmount=(int) 5000L;
		
		if(person.getAge()< 18 )
			return premiumAmount;
		else if(person.getAge() <25 && person.getAge() >18)
			premiumAmount= (int) (premiumAmount+ (long) (premiumAmount*0.1));
		else if(person.getAge() <30 && person.getAge() >25)
			premiumAmount= (int) ((premiumAmount+ (long) (premiumAmount*0.1))+(long) (premiumAmount*0.1));
		else if(person.getAge() <35 && person.getAge() >30)
			premiumAmount= (int) ((premiumAmount+ (long) (premiumAmount*0.1))+(long) (premiumAmount*0.1))+(int) (premiumAmount*0.1);
		else if(person.getAge() <40 && person.getAge() >35)
			premiumAmount= (int) ((((premiumAmount+ (long) (premiumAmount*0.1))+(long) (premiumAmount*0.1))+(long) (premiumAmount*0.1))+(long) (premiumAmount*0.1));
		else if( person.getAge() >40) {
			int ageAft40 = person.getAge()-40;
			int slabAft40= (int)ageAft40/5;
			
			premiumAmount= (int) (((((premiumAmount+ (long) (premiumAmount*0.1))+(long) (premiumAmount*0.1))+(long) (premiumAmount*0.1))+(long) (premiumAmount*0.1))+(long) (premiumAmount*slabAft40*0.1));
		}
		
		if(person.getGender() != null &&person.getGender().equals("male")) {
			premiumAmount = (int) (premiumAmount + (long) (premiumAmount *0.02));
		}
		if(person.getCurHealth() != null && person.getCurHealth().getBloodPressure().equals(true)) {
			premiumAmount = (int) (premiumAmount + (long) (premiumAmount *0.01));
		}
		if(person.getCurHealth() != null && person.getCurHealth().getBloodSugar().equals(true)) {
			premiumAmount = (int) (premiumAmount + (long) (premiumAmount *0.01));
		}
		if(person.getCurHealth() != null &&person.getCurHealth().getHypertension().equals(true)) {
			premiumAmount = (int) (premiumAmount + (long) (premiumAmount *0.01));
		}
		if(person.getCurHealth() != null &&person.getCurHealth().getOverweight().equals(true)) {
			premiumAmount = (int) (premiumAmount + (long) (premiumAmount *0.01));
		}
		
		if(person.getHabits() != null && person.getHabits().getDailyExercise().equals(true)) {
			premiumAmount = (int) (premiumAmount - (long) (premiumAmount *0.03));
		}
		
		if(person.getHabits() != null && person.getHabits().getAlcohol().equals(true)) {
			premiumAmount = (int) (premiumAmount + (long) (premiumAmount *0.03));
		}
		
		if(person.getHabits() != null && person.getHabits().getDrugs().equals(true)) {
			premiumAmount = (int) (premiumAmount + (long) (premiumAmount *0.03));
		}
		if(person.getHabits() != null && person.getHabits().getSmoking().equals(true)) {
			premiumAmount = (int) (premiumAmount + (long) (premiumAmount *0.03));
		}
		
		
		
		return premiumAmount;
			
	}

}
